swodec
======

swodec is a command-line utility to decode SWO trace data.


Requirements
------------

swodec depends on the following packages:

 - GCC (>= 4.0) or Clang
 - Make
 - pkg-config >= 0.26
 - libswo >= 0.1.0

If you're building swodec from Git, the follwing packets are additionally
required:

 - Git
 - Autoconf >= 2.69
 - Automake >= 1.11


Building and installing
-----------------------

In order to get and build the latest Git version of swodec, run the following
commands:

    $ git clone https://gitlab.zapb.de/zapb/swodec.git
    $ cd swodec
    $ ./autogen.sh
    $ ./configure
    $ make

After `make` finishes without any errors, use the following command to install
swodec:

    $ make install


Copyright and license
---------------------

swodec is licensed under the terms of the GNU General Public License (GPL),
version 3 or later. See COPYING file for details.


Website
-------

<https://gitlab.zapb.de/zapb/swodec.git>
